
[Route("api/articles")] //есть ещё контроллеры для новостей, только для другого функционала. Например, создания, редактирования и тд.
[ApiController]
public class NewsController : CustomApiController
{
    private SearchService service;
    public NewsController(DataContext context, IMapper mapper) : base (context, mapper)
    {
        service = new SearchService(context, mapper);
    }
    [HttpGet]
    [Route("")]
    public ActionResult AllNews([FromForm]SearchDto value) 
    { 
        return ProjectEntity<NewsArticleDto>(service.GetNews(value.Page, value.Search), mapper);
    }

    [HttpGet]
    [Route("{id}")]
    public ActionResult GetArticle(long id)
    {
        return ProjectEntity<NewsArticleDto>(context.NewsArticles.Where(e => e.ID == id), mapper);
    }
}

class SearchService //обращение из нескольких мест идет, извлечение из IQueryable здесь не происходит, мало ли ещё что нужно
{
    private DataContext context;
    private IMapper mapper;

    public SearchService(DataContext context, IMapper mapper)
    {
        this.context = context;
        this.mapper = mapper;
    }

    public IQueryable<NewsArticle> GetNews(int page, string search, long? category = null) 
    {
        var articles = context.NewsArticles.Where(e => !(e.IsArchive.HasValue && e.IsArchive.Value));

        if (!string.IsNullOrEmpty(search))
        {
            articles = articles.Where(ar => ar.Title.Contains(search) || ar.Content.Contains(search));
        }

        if (category.HasValue)
        {
            articles = articles.Where(ar => ar.Category != null && ar.Category.ID == category);
        }    

        return articles;
    }
}

public class CustomApiController
{
    /*
    .
    .
    .
    */
    private T Convert<TSource, T> (TSource element, long ID = 0)
        where T : IEntity
    {
        var convertedElement = mapper.Map<T>(element);
        if (ID > 0)
            convertedElement.ID = ID;
        return convertedElement;
    }

    private ActionResult ProjectEntity<T>(IQueryable source)
    {
        return Json(mapper.ProjectTo<T>(source).ToList());
    }
}

class TextReceiver // из маленькой программы, чтение строки из апишки
{
    private class Response
    {
        public string Text { get; set; }
    }

    private readonly string api;
    private readonly string headerName;
    private readonly string headerValue;

    public TextReceiver(string api, string headerName, string headerValue)
    {
        this.api = api;
        this.headerName = headerName;
        this.headerValue = headerValue;
    }

    public async Task<string> ReadLine(int id)
    {
        var request = (HttpWebRequest)WebRequest.Create(api + id);
        request.Headers[headerName] = headerValue;
        request.AutomaticDecompression = DecompressionMethods.GZip;
        var result = string.Empty;
        using (var response = await request.GetResponseAsync())
        using (var stream = response.GetResponseStream())
        using (var reader = new StreamReader(stream))
        {
            return JsonSerializer.Deserialize<Response>(reader.ReadToEnd()).Text;
        }
    }
}

//К сожалению, примеров мало
//я предпочитаю иметь много классов с закрытым функционалом, иметь малую связность
//в Unity использую немного другой кодстайл для приватных полей\свойств, так как игровые сущности наследуются от MonoBehaviour, а от него очень много передается ребенку
public class PlayerMover : MonoBehaviour
{
    [SerializeField] float _speed;
    private Rigidbody2D _rb;
    private Vector3 _newPosition;
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _newPosition = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));            
    }

    private void FixedUpdate()
    {
        _rb.MovePosition(transform.position + _newPosition * Time.fixedDeltaTime * _speed);
    }
}

// ленивый и простой параллакс эффект для игры. Извне не трогается, только получает рендерер и материал
[RequireComponent(typeof(MeshRenderer))]
public class Parallaxing : MonoBehaviour
{
    [SerializeField] private float _speed = 1f;
    private MeshRenderer _meshRenderer;
    private Material _material;
    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _material = _meshRenderer.material;
    }

    private void Update()
    {
        _material.mainTextureOffset += Vector2.up * Time.deltaTime * _speed;
    }
}
//хотя вряд ли что демонстрирует, но многие популярные примеры на юнити кишат паблик переменными, 
//имеют много логики внутри себя и тд и уже на первых итерациях создаются проблемы с расширяемостью


//скорее всего, тут так себе все раскрывается, могу выполнить тестовую задачку, будет яснее

